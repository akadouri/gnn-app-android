//
//  ResourcesTableViewController.m
//  GNN
//
//  Created by Martin Johansson on 2013-08-05.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import "ResourcesTableViewController.h"

@interface ResourcesTableViewController ()

@end

@implementation ResourcesTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    self.view.backgroundColor = [UIColor colorWithRed:(175/255.0)  green:(175/255.0)  blue:(175/255.0)  alpha:1.0];
    
    [super viewDidLoad];
    
}


#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GNN_SingleButton.png"]];
    
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}


#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"websiteSegue"]) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"website" forKey:@"siteSelector"];
        
    } else if (([[segue identifier] isEqualToString:@"icSegue"])) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"ic" forKey:@"siteSelector"];
        
    } else if (([[segue identifier] isEqualToString:@"learnednessSegue"])) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"learnedness" forKey:@"siteSelector"];
        
    } else if (([[segue identifier] isEqualToString:@"turnitinSegue"])) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"turnitin" forKey:@"siteSelector"];
        
    } else if (([[segue identifier] isEqualToString:@"edmodoSegue"])) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"edmodo" forKey:@"siteSelector"];
        
    }
}


#pragma mark - Action Methods

- (IBAction)goBackAct:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end

