//
//  HomeTableViewController.m
//  GNN
//
//  Created by Martin Johansson on 2013-07-28.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import "HomeTableViewController.h"

@interface HomeTableViewController ()

@end

@implementation HomeTableViewController
@synthesize startImg, startImgV;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    self.tableView.userInteractionEnabled=NO;
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage *navBarImg = [UIImage imageNamed:@"GNN_NavBar"];
    [navBar setBackgroundImage:navBarImg forBarMetrics:UIBarMetricsDefault];
    
    self.view.backgroundColor = [UIColor colorWithRed:(175/255.0)  green:(175/255.0)  blue:(175/255.0)  alpha:1.0];
    
    int height =  [[UIScreen mainScreen] bounds].size.height;
    
    if(height > 480){
        
        startImg = [UIImage imageNamed:@"launch-568h@2x.png"];
        
    } else {
        
        startImg = [UIImage imageNamed:@"launch.png"];
        
    }
    
    startImgV = [[UIImageView alloc] initWithImage:startImg];
    startImgV.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-40);
    [self.view addSubview:startImgV];
    
    
    [UIImageView animateWithDuration:1.25
                               delay:2.5
                             options:UIViewAnimationOptionCurveEaseInOut
                          animations:^{
                              startImgV.frame = CGRectOffset(startImgV.frame, 0, -self.view.frame.size.height);
                          }
                          completion:^(BOOL finished){
                              [startImgV removeFromSuperview];
                              self.tableView.userInteractionEnabled=YES;
                          }];
    
    [super viewDidLoad];
    
    NSLog(@"First view loaded!");

}


#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GNN_SingleButton.png"]];
    
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"Did Select Row At IndexPath:%i", indexPath.row);
    
    if (indexPath.row==5) {
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self displayComposerSheet];
    }
    
}

#pragma mark - Mail Composer Delegate


-(void)displayComposerSheet {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        
        [picker setSubject:@"GNN App Suggestions"];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"gnnapp@gmail.com",
                                 nil];
        [picker setToRecipients:toRecipients];
        
        NSString *emailBody = @"";
        [picker setMessageBody:emailBody isHTML:NO];
        
        [self presentViewController:picker animated:YES completion:nil];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can not send mail!"
                                                        message:@"Please check that you have setup mail on your device"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setStartImg:nil];
    [self setStartImgV:nil];
    
    [super viewDidUnload];
}



@end
