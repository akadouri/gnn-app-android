//
//  AppDelegate.m
//  GNN
//
//  Created by Martin Johansson on 2013-07-28.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate
@synthesize lastUpdated;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [self performSelectorInBackground:@selector(updateRestImage) withObject:nil];
   
}

-(void)updateRestImage {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSURL *dataUpdateURL = [NSURL
                            URLWithString:@"http://www.amase.se/phpGNNupdateDate.php"];
    
    NSMutableURLRequest *requestUpdate = [NSMutableURLRequest requestWithURL:dataUpdateURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5];
    
    updateConnection = [[NSURLConnection alloc] initWithRequest:requestUpdate delegate:self];
    
    if (updateConnection) {
        NSString *updateString = [NSString stringWithContentsOfURL:dataUpdateURL
                                                          encoding:NSASCIIStringEncoding
                                                             error:nil];
        
        lastUpdated = [defaults objectForKey:@"updatedDate"];
        
        NSLog(@"lastUpdated:%@", lastUpdated);
        NSLog(@"updateString:%@", updateString);
        
        if (updateString==nil) {
            NSLog(@"No response");
        } else {
            
            if ([lastUpdated isEqualToString:updateString]) {
                
                NSLog(@"Images already updated");
                
            } else {
                
                NSURL *imgUrl1 = [NSURL
                                  URLWithString:@"http://www.amase.se/GNNimageEven.png"];
                
                NSMutableURLRequest *urlImgRequest1 = [NSMutableURLRequest requestWithURL:imgUrl1 cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
                
                img1Connection = [[NSURLConnection alloc] initWithRequest:urlImgRequest1 delegate:self];
                
                if (img1Connection) {
                    NSData *imgDataEven = [NSData dataWithContentsOfURL:imgUrl1];
                    [defaults setObject:imgDataEven forKey:@"featuredRestImgEven"];
                } else {
                    NSLog(@"img1Connection Failed!");
                }
                
                
                NSURL *imgUrl2 = [NSURL
                                  URLWithString:@"http://www.amase.se/GNNimageOdd.png"];
                
                NSMutableURLRequest *urlImgRequest2 = [NSMutableURLRequest requestWithURL:imgUrl2 cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
                
                img2Connection = [[NSURLConnection alloc] initWithRequest:urlImgRequest2 delegate:self];
                
                if (img1Connection) {
                    NSData *imgDataOdd = [NSData dataWithContentsOfURL:imgUrl2];
                    [defaults setObject:imgDataOdd forKey:@"featuredRestImgOdd"];
                } else {
                    NSLog(@"img1Connection Failed!");
                }
                
                [defaults setObject:updateString forKey:@"updatedDate"];
                [defaults synchronize];
                
                NSLog(@"Images was updated");
                
            }
        }
    } else {
        NSLog(@"updateConnection Failed!");
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
