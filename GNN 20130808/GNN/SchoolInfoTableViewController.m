//
//  SchoolInfoTableViewController.m
//  GNN
//
//  Created by Martin Johansson on 2013-07-28.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import "SchoolInfoTableViewController.h"

@interface SchoolInfoTableViewController ()

@end

@implementation SchoolInfoTableViewController
@synthesize mailRecStrg;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    self.view.backgroundColor = [UIColor colorWithRed:(175/255.0)  green:(175/255.0)  blue:(175/255.0)  alpha:1.0];
    
    [super viewDidLoad];

}


#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GNN_SingleButton.png"]];
    
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 20)];
    headerView.backgroundColor = [UIColor colorWithRed:(1/255.0)  green:(14/255.0)  blue:(137/255.0)  alpha:1.0];
    
    UILabel *headerLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 200, 20)];
    headerLbl.backgroundColor = [UIColor clearColor];
    headerLbl.font = [UIFont boldSystemFontOfSize:14];
    headerLbl.textColor = [UIColor colorWithRed:(252/255.0)  green:(149/255.0)  blue:(0/255.0)  alpha:1.0];
    [headerView addSubview:headerLbl];
    
    if (section==0) {
        headerLbl.text = @"Offices";
    } else {
        headerLbl.text = @"Administration";
    }
    
    return headerView;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"Did Select Row At IndexPath:%i", indexPath.row);
    
    switch (indexPath.section) {
        case 0:
            if (indexPath.row==0) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164414700"]];
            } else if (indexPath.row==1) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164414797"]];
            } else if (indexPath.row==2) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164414720"]];
            }
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            break;
        case 1:
            if (indexPath.row==0) {
                mailRecStrg = @"bkaplan@greatneck.k12.ny.us";
            } else if (indexPath.row==1) {
                mailRecStrg = @"phugo@greatneck.k12.ny.us";
            } else if (indexPath.row==2) {
                mailRecStrg = @"dkrauz@greatneck.k12.ny.us";
            } else if (indexPath.row==3) {
                mailRecStrg = @"rlevine@greatneck.k12.ny.us";
            }
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [self displayComposerSheet];
            break;
        default:
            mailRecStrg = @"";
            break;
    }
}


#pragma mark - Mail Composer Delegate

-(void)displayComposerSheet {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        
        [picker setSubject:@""];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:mailRecStrg,
                                 nil];
        [picker setToRecipients:toRecipients];
        
        NSString *emailBody = @"";
        [picker setMessageBody:emailBody isHTML:NO];
        
        [self presentViewController:picker animated:YES completion:nil];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can not send mail!"
                                                        message:@"Please check that you have setup mail on your device"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Action Methods

- (IBAction)goBackAct:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setMailRecStrg:nil];
    
    [super viewDidUnload];
}

@end
