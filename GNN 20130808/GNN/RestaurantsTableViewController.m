//
//  RestaurantsTableViewController.m
//  GNN
//
//  Created by Martin Johansson on 2013-07-29.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import "RestaurantsTableViewController.h"

@interface RestaurantsTableViewController ()

@end

@implementation RestaurantsTableViewController
@synthesize featuredRestaurantImgV;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    
    self.view.backgroundColor = [UIColor colorWithRed:(175/255.0)  green:(175/255.0)  blue:(175/255.0)  alpha:1.0];
    
    [super viewDidLoad];
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    [self performSelector:@selector(showFeaturedRestImg) withObject:nil afterDelay:0.1f];
    
}

-(void)showFeaturedRestImg {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setMinimumDaysInFirstWeek:4];
    NSDate *date = [NSDate date];
    NSLog(@"week: %i", [[calendar components: NSWeekOfYearCalendarUnit fromDate:date] weekOfYear]);
    
    if ([[calendar components: NSWeekOfYearCalendarUnit fromDate:date] weekOfYear] % 2) {
        NSData *imgDataOdd1 = [defaults objectForKey:@"featuredRestImgOdd"];
        featuredRestaurantImgV.image = [UIImage imageWithData:imgDataOdd1];
    } else {
        NSData *imgDataEven1 = [defaults objectForKey:@"featuredRestImgEven"];
        featuredRestaurantImgV.image = [UIImage imageWithData:imgDataEven1];
    }
    
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:0 inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationTop];
    [self.tableView setContentOffset:CGPointZero animated:YES];
    
    NSLog(@"featuredRestImg did load!");
    
}


#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==0) {
        cell.backgroundColor = [UIColor colorWithRed:(175/255.0)  green:(175/255.0)  blue:(175/255.0)  alpha:1.0];
    } else {
        cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GNN_SingleButton.png"]];
    }
    
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 20)];
    headerView.backgroundColor = [UIColor colorWithRed:(1/255.0)  green:(14/255.0)  blue:(137/255.0)  alpha:1.0];
    
    UILabel *headerLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 200, 20)];
    headerLbl.backgroundColor = [UIColor clearColor];
    headerLbl.font = [UIFont boldSystemFontOfSize:14];
    headerLbl.textColor = [UIColor colorWithRed:(252/255.0)  green:(149/255.0)  blue:(0/255.0)  alpha:1.0];
    [headerView addSubview:headerLbl];
    
    if (section==1) {
        headerLbl.text = @"Delivery";
    } else if (section==2){
        headerLbl.text = @"Near Campus";
    } else {
        headerLbl.text = @"";
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        if (featuredRestaurantImgV.image != nil) {
            return 400;
        } else {
            return 0;
        }
    } else {
        
        return 71;
    }
}
/*
- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation {
    
    
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"Did Select Row At IndexPath:%i", indexPath.row);
    
    switch (indexPath.section) {
        case 0:
            break;
        case 1:
            if (indexPath.row==0) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5165041199"]];
            } else if (indexPath.row==1) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164871122"]];
            } else if (indexPath.row==2) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164828939"]];
            }
            break;
        case 2:
            if (indexPath.row==0) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164821510"]];
            } else if (indexPath.row==1) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5167737340"]];
            } else if (indexPath.row==2) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5167089389"]];
            } else if (indexPath.row==3) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164823335"]];
            } else if (indexPath.row==4) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164823335"]];
            } else if (indexPath.row==5) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164877440"]];
            } else if (indexPath.row==6) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5165041199"]];
            } else if (indexPath.row==7) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164664224"]];
            } else if (indexPath.row==8) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164828939"]];
            } else if (indexPath.row==9) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164663003"]];
            } else if (indexPath.row==10) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164670505"]];
            } else if (indexPath.row==11) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5167089880"]];
            } else if (indexPath.row==12) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164872228"]];
            } else if (indexPath.row==13) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5164395144"]];
            } else if (indexPath.row==14) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5167734600"]];
            } else if (indexPath.row==15) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://5167736328"]];
            }
            break;
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Action Methods

- (IBAction)goBackAct:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setFeaturedRestaurantImgV:nil];
    [super viewDidUnload];
}
    
@end
