//
//  AppDelegate.h
//  GNN
//
//  Created by Martin Johansson on 2013-07-28.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    NSURLConnection *updateConnection;
    NSURLConnection *img1Connection;
    NSURLConnection *img2Connection;
}

@property (strong, nonatomic) UIWindow *window;

@property (weak, nonatomic) NSMutableString *lastUpdated;

@end
