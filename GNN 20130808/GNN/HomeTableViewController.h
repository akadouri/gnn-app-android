//
//  HomeTableViewController.h
//  GNN
//
//  Created by Martin Johansson on 2013-07-28.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface HomeTableViewController : UITableViewController <MFMailComposeViewControllerDelegate>

@property (nonatomic, weak) UIImage *startImg;
@property (nonatomic, strong) UIImageView *startImgV;



@end
