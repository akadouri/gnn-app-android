//
//  ScheduleModalViewController.h
//  GNN
//
//  Created by Martin Johansson on 2013-07-31.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleModalViewController : UIViewController


- (IBAction)dismissModalVC:(id)sender;


@end
