//
//  ContactForeignLanguageTableViewController.m
//  GNN
//
//  Created by Martin Johansson on 2013-07-30.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import "ContactForeignLanguageTableViewController.h"

@interface ContactForeignLanguageTableViewController ()

@end

@implementation ContactForeignLanguageTableViewController
@synthesize mailRecStrg;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    self.view.backgroundColor = [UIColor colorWithRed:(175/255.0)  green:(175/255.0)  blue:(175/255.0)  alpha:1.0];
    
    [super viewDidLoad];
    
}


#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GNN_SingleButton.png"]];
    
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Did Select Row At IndexPath:%i", indexPath.row);
    
    if (indexPath.row==0) {
        mailRecStrg = @"mdeluccia@greatneck.k12.ny.us";
    } else if (indexPath.row==1) {
        mailRecStrg = @"jasvestas@greatneck.k12.ny.us";
    } else if (indexPath.row==2) {
        mailRecStrg = @"pchen@greatneck.k12.ny.us";
    } else if (indexPath.row==3) {
        mailRecStrg = @"cgorta@greatneck.k12.ny.us";
    } else if (indexPath.row==4) {
        mailRecStrg = @"ihenriquez@greatneck.k12.ny.us";
    } else if (indexPath.row==5) {
        mailRecStrg = @"civry@greatneck.k12.ny.us";
    } else if (indexPath.row==6) {
        mailRecStrg = @"bkarinikolas@greatneck.k12.ny.us";
    } else if (indexPath.row==7) {
        mailRecStrg = @"lkrowne@greatneck.k12.ny.us";
    } else if (indexPath.row==8) {
        mailRecStrg = @"jlynch@greatneck.k12.ny.us";
    } else if (indexPath.row==9) {
        mailRecStrg = @"kmcaleer@greatneck.k12.ny.us";
    } else if (indexPath.row==10) {
        mailRecStrg = @"dvillacorta@greatneck.k12.ny.us";
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self displayComposerSheet];
    
}


#pragma mark - Mail Composer Delegate

-(void)displayComposerSheet {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        
        [picker setSubject:@""];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:mailRecStrg,
                                 nil];
        [picker setToRecipients:toRecipients];
        
        NSString *emailBody = @"";
        [picker setMessageBody:emailBody isHTML:NO];
        
        [self presentViewController:picker animated:YES completion:nil];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can not send mail!"
                                                        message:@"Please check that you have setup mail on your device"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Action Methods

- (IBAction)goBackAct:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setMailRecStrg:nil];
    
    [super viewDidUnload];
}

@end
