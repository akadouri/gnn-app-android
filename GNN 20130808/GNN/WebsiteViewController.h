//
//  WebsiteViewController.h
//  GNN
//
//  Created by Martin Johansson on 2013-07-28.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebsiteViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *loadImgV;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actInd;
@property (weak, nonatomic) IBOutlet UILabel *loadLbl;
@property (weak, nonatomic) IBOutlet UIButton *webBackBtn;
@property (weak, nonatomic) IBOutlet UIButton *webFwdBtn;

- (IBAction)goBackAct:(id)sender;

@end
