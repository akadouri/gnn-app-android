//
//  RestaurantsTableViewController.h
//  GNN
//
//  Created by Martin Johansson on 2013-07-29.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface RestaurantsTableViewController : UITableViewController <MFMailComposeViewControllerDelegate> {
    
    NSURLConnection *connection;
    
}


- (IBAction)goBackAct:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *featuredRestaurantImgV;



@end
