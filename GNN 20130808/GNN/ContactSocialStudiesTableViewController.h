//
//  ContactSocialStudiesTableViewController.h
//  GNN
//
//  Created by Martin Johansson on 2013-07-29.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ContactSocialStudiesTableViewController : UITableViewController <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) NSString *mailRecStrg;

- (IBAction)goBackAct:(id)sender;

@end
