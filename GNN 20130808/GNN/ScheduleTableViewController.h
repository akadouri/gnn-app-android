//
//  ScheduleTableViewController.h
//  GNN
//
//  Created by Martin Johansson on 2013-07-28.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleTableViewController : UITableViewController


- (IBAction)goBackAct:(id)sender;

@end
