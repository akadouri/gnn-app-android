//
//  ScheduleModalViewController.m
//  GNN
//
//  Created by Martin Johansson on 2013-07-31.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import "ScheduleModalViewController.h"

@interface ScheduleModalViewController ()

@end

@implementation ScheduleModalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}


#pragma mark - DissmissVC Action

- (IBAction)dismissModalVC:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
    
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
