//
//  ContactForeignLanguageTableViewController.h
//  GNN
//
//  Created by Martin Johansson on 2013-07-30.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ContactForeignLanguageTableViewController : UITableViewController <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) NSString *mailRecStrg;

- (IBAction)goBackAct:(id)sender;

@end
