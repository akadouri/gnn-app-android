//
//  WebsiteViewController.m
//  GNN
//
//  Created by Martin Johansson on 2013-07-28.
//  Copyright (c) 2013 aMASE. All rights reserved.
//

#import "WebsiteViewController.h"

@interface WebsiteViewController ()

@end

@implementation WebsiteViewController
@synthesize loadImgV, webView;
@synthesize actInd, loadLbl;
@synthesize webBackBtn, webFwdBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    webView.delegate=self;
    
    self.view.backgroundColor = [UIColor colorWithRed:(175/255.0)  green:(175/255.0)  blue:(175/255.0)  alpha:1.0];
    
    webView.alpha=0;
    
    int height =  [[UIScreen mainScreen] bounds].size.height;
    
    if(height > 480){
        
        loadImgV.image = [UIImage imageNamed:@"launch-568h@2x.png"];
        
    } else {
        
        loadImgV.image = [UIImage imageNamed:@"launch.png"];
        
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"website"]) {
        
        loadLbl.text=@"Loading Website...";
        
    } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"ic"]) {
        
        loadLbl.text=@"Loading IC Parent Portal...";
        
    } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"learnedness"]) {
        
        loadLbl.text=@"Loading Learnedness...";
        
    } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"turnitin"]) {
        
        loadLbl.text=@"Loading Turnitin...";
        
    } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"edmodo"]) {
        
        loadLbl.text=@"Loading Edmodo...";
        
    }
    
    [actInd startAnimating];
    
    webBackBtn.enabled = NO;
    webFwdBtn.enabled = NO;
    
    if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"website"]) {
        NSString *urlAddress = @"http://www.greatneck.k12.ny.us";
        NSURL *url = [NSURL URLWithString:urlAddress];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        [webView loadRequest:requestObj];
    } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"ic"]) {
        NSString *urlAddress = @"https://ic.greatneck.k12.ny.us/campus/portal/greatneck.jsp";
        NSURL *url = [NSURL URLWithString:urlAddress];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        [webView loadRequest:requestObj];
    } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"learnedness"]) {
        NSString *urlAddress = @"http://learnedness.net/login/index.php";
        NSURL *url = [NSURL URLWithString:urlAddress];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        [webView loadRequest:requestObj];
    } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"turnitin"]) {
        NSString *urlAddress = @"https://turnitin.com/login_page.asp?err=1&lang=en_us";
        NSURL *url = [NSURL URLWithString:urlAddress];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        [webView loadRequest:requestObj];
    } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"edmodo"]) {
        NSString *urlAddress = @"https://www.edmodo.com/m/";
        NSURL *url = [NSURL URLWithString:urlAddress];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        [webView loadRequest:requestObj];
    } 
    
    [super viewDidLoad];
}


#pragma mark - WebView Delegate

- (void)webViewDidStartLoad:(UIWebView *)aWebView {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    
    if (actInd.isAnimating) {
        [actInd stopAnimating];
        
        [UIImageView animateWithDuration:2.75
                                   delay:0
                                 options:UIViewAnimationOptionCurveEaseInOut
                              animations:^{
                                  actInd.alpha=0;
                                  loadLbl.alpha=0;
                                  loadImgV.alpha=0;
                                  webView.alpha=1;
                              }
         
                              completion:^(BOOL finished){
                                  
                                  NSLog(@"Load animation finished!");
                                  
                              }];
    }
    
    
    if (webView.canGoBack) {
        webBackBtn.enabled=YES;
    } else {
        webBackBtn.enabled=NO;
    }
    
    if (webView.canGoForward) {
        webFwdBtn.enabled=YES;
    } else {
        webFwdBtn.enabled=NO;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    NSLog(@"webView loaded!");
    
}

- (void)webView:(UIWebView *)aWebView didFailLoadWithError:(NSError *)error{
    
    if ([error code] != NSURLErrorCancelled) {
        NSLog(@"could not load the url caused by error: %@", error);
        
        [self showAlert];
    }
    
}


#pragma mark - Alert View

- (void) showAlert {
    
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Failed to load URL"
                                                      message:@"Please check your connection!"
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Try again", nil];
    [message show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Cancel"])
    {
        NSLog(@"Alert:Cancel");
        
        [actInd stopAnimating];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"website"]) {
            
            loadLbl.text = @"Could not load Website";
            
        } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"ic"]) {
            
            loadLbl.text = @"Could not load IC Parent Portal";
            
        } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"learnedness"]) {
            
            loadLbl.text = @"Could not load Learnedness";
            
        } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"turnitin"]) {
            
            loadLbl.text = @"Could not load Turnitin";
            
        } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"edmodo"]) {
            
            loadLbl.text = @"Could not load Edmodo";
            
        }
        
        [UIImageView animateWithDuration:2.75
                                   delay:0
                                 options:UIViewAnimationOptionCurveEaseInOut
                              animations:^{
                                  actInd.alpha=1;
                                  loadLbl.alpha=1;
                                  loadImgV.alpha=1;
                                  webView.alpha=0;
                              }
         
                              completion:^(BOOL finished){
                                  
                                  NSLog(@"Load animation finished!");
                                  
                              }];
        
        
    } else if([title isEqualToString:@"Try again"]) {
        
        NSLog(@"Alert:Try again");
        
        [actInd startAnimating];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"website"]) {
            NSString *urlAddress = @"http://www.greatneck.k12.ny.us";
            NSURL *url = [NSURL URLWithString:urlAddress];
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            
            [webView loadRequest:requestObj];
        } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"ic"]) {
            NSString *urlAddress = @"https://ic.greatneck.k12.ny.us/campus/portal/greatneck.jsp";
            NSURL *url = [NSURL URLWithString:urlAddress];
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            
            [webView loadRequest:requestObj];
        } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"learnedness"]) {
            NSString *urlAddress = @"http://learnedness.net/login/index.php";
            NSURL *url = [NSURL URLWithString:urlAddress];
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            
            [webView loadRequest:requestObj];
        } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"turnitin"]) {
            NSString *urlAddress = @"https://turnitin.com/login_page.asp?err=1&lang=en_us";
            NSURL *url = [NSURL URLWithString:urlAddress];
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            
            [webView loadRequest:requestObj];
        } else if ([[defaults objectForKey:@"siteSelector"] isEqualToString:@"edmodo"]) {
            NSString *urlAddress = @"https://www.edmodo.com/m/";
            NSURL *url = [NSURL URLWithString:urlAddress];
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            
            [webView loadRequest:requestObj];
        }
    }
}


#pragma mark - Action Methods

- (IBAction)goBackAct:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [self setLoadImgV:nil];
    [self setActInd:nil];
    [self setLoadLbl:nil];
    [self setWebBackBtn:nil];
    [self setWebFwdBtn:nil];
    
    [super viewDidUnload];
}




@end
