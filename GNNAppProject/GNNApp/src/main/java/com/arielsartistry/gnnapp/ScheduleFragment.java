package com.arielsartistry.gnnapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by Ariel on 9/25/13.
 */
public class ScheduleFragment extends Fragment {

    private String type;

    public ScheduleFragment(String type)
    {
        this.type = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        int resId;
        if(type.equals("Ordinary"))
        {
            resId = R.layout.ordinary_schedule;
        }else if(type.equals("Advisory")){
            resId = R.layout.advisory_schedule;
        }else
        {
            resId = R.layout.assembly_schedule;
        }
        View view = inflater.inflate(resId, null);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        return view;
    }
}
