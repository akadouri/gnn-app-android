package com.arielsartistry.gnnapp;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Ariel on 9/25/13.
 */
public class BellScheduleSelectorFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ListView view = new ListView(getActivity());
        view.setBackgroundColor(getActivity().getResources().getColor(R.color.background));
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        ArrayList<String> items = new ArrayList<String>();
        items.add("Ordinary Schedule");
        items.add("Advisory Schedule");
        items.add("Assembly Schedule");

        BellListAdapter adapter = new BellListAdapter(getActivity(), android.R.layout.simple_list_item_1, items);
        view.setAdapter(adapter);

        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                FragmentTransaction transaction = BellScheduleSelectorFragment.this.getActivity().getSupportFragmentManager().beginTransaction();
                switch(i)
                {
                    case 0:
                        transaction.replace(R.id.container_layout, new ScheduleFragment("Ordinary"));
                        transaction.addToBackStack(null);
                        break;
                    case 1:
                        transaction.replace(R.id.container_layout, new ScheduleFragment("Advisory"));
                        transaction.addToBackStack(null);
                        break;
                    case 2:
                        transaction.replace(R.id.container_layout, new ScheduleFragment("Assembly"));
                        transaction.addToBackStack(null);
                        break;
                }
                transaction.commit();
            }
        });

        return view;
    }

    private class BellListAdapter extends ArrayAdapter<String> {

        private ArrayList<String> items;

        public BellListAdapter(Context context, int textViewResourceId, ArrayList<String> items)
        {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public int getCount() { return items.size(); }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent){
            LayoutInflater vi = (LayoutInflater) BellScheduleSelectorFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.standard_row, null);

            ((TextView) v.findViewById(R.id.textView)).setText(items.get(position));
            return v;
        }
    }
}