package com.arielsartistry.gnnapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Ariel on 9/26/13.
 */
public class NearbyRestaurantsFragment extends Fragment {

    private String[] number;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ListView listView = new ListView(getActivity());
        listView.setBackgroundColor(getResources().getColor(R.color.background));
        listView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        String[] name = getResources().getStringArray(R.array.nearby_restaurants_name);
        number = getResources().getStringArray(R.array.nearby_restaurants_number);

        NearbyRestaurantsAdapter adapter = new NearbyRestaurantsAdapter(getActivity(), android.R.layout.simple_list_item_1, name, number);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + number[i]));
                startActivity(callIntent);
            }
        });

        return listView;
    }

    private class NearbyRestaurantsAdapter extends ArrayAdapter<String> {

        private String[] name;
        private String[] email;

        public NearbyRestaurantsAdapter(Context context, int textViewResourceId, String[] name, String[] email)
        {
            super(context, textViewResourceId, name);
            this.name = name;
            this.email = email;
        }

        @Override
        public int getCount() { return name.length; }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent){
            LayoutInflater vi = (LayoutInflater) NearbyRestaurantsFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.contact_row, null);
            ((ImageView) v.findViewById(R.id.email)).setImageResource(R.drawable.call_button);

            if(!email[position].equals(""))
            {
                ((TextView) v.findViewById(R.id.textView)).setText(name[position]);
            }
            return v;
        }
    }
}
