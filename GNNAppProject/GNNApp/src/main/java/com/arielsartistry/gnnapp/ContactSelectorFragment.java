package com.arielsartistry.gnnapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Ariel on 9/26/13.
 */
public class ContactSelectorFragment extends Fragment {

    private String department;
    private  String[] name = {}, email = {};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        department = getArguments().getString("key");

        ListView listView = new ListView(getActivity());
        listView.setBackgroundColor(getResources().getColor(R.color.background));
        listView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        listView.setClickable(true);

        if(department.equals("Guidance"))
        {
            name = getResources().getStringArray(R.array.guidance_name);
            email = getResources().getStringArray(R.array.guidance_email);
        }else if(department.equals("Mathematics"))
        {
            name = getResources().getStringArray(R.array.math_name);
            email = getResources().getStringArray(R.array.math_email);
        }else if(department.equals("Science"))
        {
            name = getResources().getStringArray(R.array.science_name);
            email = getResources().getStringArray(R.array.science_email);
        }else if(department.equals("English"))
        {
            name = getResources().getStringArray(R.array.english_name);
            email = getResources().getStringArray(R.array.english_email);
        }else if(department.equals("Social Studies"))
        {
            name = getResources().getStringArray(R.array.social_studies_name);
            email = getResources().getStringArray(R.array.social_studies_email);
        }else if(department.equals("Foreign Language"))
        {
            name = getResources().getStringArray(R.array.foreign_language_name);
            email = getResources().getStringArray(R.array.foreign_language_email);
        }

        DepartmentContactListAdapter adapter = new DepartmentContactListAdapter(getActivity(), android.R.layout.simple_list_item_1, name, email);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(email[i] != "")
                {
                startActivity(Intent.createChooser(new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", email[i] + "@greatneck.k12.ny.us", null)), "Send email..."));
                }
            }
        });
        return listView;
    }

    private class DepartmentContactListAdapter extends ArrayAdapter<String> {

        private String[] name;
        private String[] email;

        public DepartmentContactListAdapter(Context context, int textViewResourceId, String[] name, String[] email)
        {
            super(context, textViewResourceId, name);
            this.name = name;
            this.email = email;
        }

        @Override
        public int getCount() { return name.length; }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent){
            LayoutInflater vi = (LayoutInflater) ContactSelectorFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.contact_row, null);

            ((TextView) v.findViewById(R.id.textView)).setText(name[position]);
            if(email[position].equals(""))
            {
                v.findViewById(R.id.email).setVisibility(View.GONE);
            }

            return v;
        }
    }
}