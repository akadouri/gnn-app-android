package com.arielsartistry.gnnapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends FragmentActivity {

    private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.listView);

        ArrayList<String> items = new ArrayList<String>();
        items.add("School Information");
        items.add("Bell Schedule");
        items.add("Contact Teachers");
        items.add("Nearby Restaurants");
        items.add("Resources");
        items.add("Guide Post Online");
        items.add("Suggestions");

        HomeListAdapter adapter = new HomeListAdapter(this, android.R.layout.simple_list_item_1, items);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                switch(i)
                {
                    case 5:
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://guidepostonline.org/")));
                        return;
                    case 6:
                        startActivity(Intent.createChooser(new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                "mailto", "gnnapp+android@gmail.com", null)), "Email suggestion..."));
                        return;
                }
                FragmentTransaction transaction = MainActivity.this.getSupportFragmentManager().beginTransaction();
                switch(i)
                {
                    case 0:
                        transaction.replace(R.id.container_layout, new SchoolInfoFragment());
                        transaction.addToBackStack(null);
                        break;
                    case 1:
                        transaction.replace(R.id.container_layout, new BellScheduleSelectorFragment());
                        transaction.addToBackStack(null);
                        break;
                    case 2:
                        transaction.replace(R.id.container_layout, new DepartmentSelectorFragment());
                        transaction.addToBackStack(null);
                        break;
                    case 3:
                        transaction.replace(R.id.container_layout, new NearbyRestaurantsFragment());
                        transaction.addToBackStack(null);
                        break;
                    case 4:
                        transaction.replace(R.id.container_layout, new ResourcesFragment());
                        transaction.addToBackStack(null);
                        break;
                }
                transaction.commit();
            }
        });
    }


   /* @Override Don't need an options menu right now
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }*/

    private class HomeListAdapter extends ArrayAdapter<String> {

        private ArrayList<String> items;

        public HomeListAdapter(Context context, int textViewResourceId, ArrayList<String> items)
        {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public int getCount() { return items.size(); }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent){
            LayoutInflater vi = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.standard_row, null);

            ((TextView) v.findViewById(R.id.textView)).setText(items.get(position));
            return v;
        }
    }
}