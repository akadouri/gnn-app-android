package com.arielsartistry.gnnapp;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Ariel on 9/25/13.
 */
public class SchoolInfoFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.school_info, null);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        view.findViewById(R.id.btn_main_office).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:15164414700"));
                startActivity(callIntent);
            }
        });

        view.findViewById(R.id.btn_nurse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:15164414797"));
                startActivity(callIntent);
            }
        });

        view.findViewById(R.id.btn_guidance).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:15164414720"));
                startActivity(callIntent);
            }
        });

        view.findViewById(R.id.btn_kalpan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(Intent.createChooser(new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "bkaplan@greatneck.k12.ny.us", null)), "Send email..."));
            }
        });

        view.findViewById(R.id.btn_hugo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(Intent.createChooser(new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "phugo@greatneck.k12.ny.us", null)), "Send email..."));
            }
        });

        view.findViewById(R.id.btn_krauz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(Intent.createChooser(new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "dkrauz@greatneck.k12.ny.us", null)), "Send email..."));
            }
        });

        view.findViewById(R.id.btn_levine).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(Intent.createChooser(new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "rlevine@greatneck.k12.ny.us", null)), "Send email..."));
            }
        });
        return view;
    }
}
