package com.arielsartistry.gnnapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Ariel on 9/26/13.
 */
public class ResourcesFragment extends Fragment {

    String[] name, link;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        name = getResources().getStringArray(R.array.resources_title);
        link = getResources().getStringArray(R.array.resources_link);

        ListView listView = new ListView(getActivity());
        listView.setBackgroundColor(getResources().getColor(R.color.background));
        listView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        listView.setClickable(true);

        DepartmentContactListAdapter adapter = new DepartmentContactListAdapter(getActivity(), android.R.layout.simple_list_item_1, name);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link[i])));
            }
        });
        return listView;
    }

    private class DepartmentContactListAdapter extends ArrayAdapter<String> {

        private String[] name;

        public DepartmentContactListAdapter(Context context, int textViewResourceId, String[] name)
        {
            super(context, textViewResourceId, name);
            this.name = name;
        }

        @Override
        public int getCount() { return name.length; }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent){
            LayoutInflater vi = (LayoutInflater) ResourcesFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View v = vi.inflate(R.layout.contact_row, null);
            v.findViewById(R.id.email).setVisibility(View.GONE);
            ((TextView) v.findViewById(R.id.textView)).setText(name[position]);

            return v;
        }
    }
}
