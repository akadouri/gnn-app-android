package com.arielsartistry.gnnapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Ariel on 9/26/13.
 */
public class DepartmentSelectorFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ListView view = new ListView(getActivity());
        view.setBackgroundColor(getActivity().getResources().getColor(R.color.background));
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        ArrayList<String> items = new ArrayList<String>();
        items.add("Guidance");
        items.add("Mathematics");
        items.add("Science");
        items.add("English");
        items.add("Social Studies");
        items.add("Foreign Language");

        DepartmentListAdapter adapter = new DepartmentListAdapter(getActivity(), android.R.layout.simple_list_item_1, items);
        view.setAdapter(adapter);

        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                FragmentTransaction transaction = DepartmentSelectorFragment.this.getActivity().getSupportFragmentManager().beginTransaction();

                Bundle bundle = new Bundle();
                switch(i)
                {
                    case 0:
                        bundle.putString("key", "Guidance");
                        break;
                    case 1:
                        bundle.putString("key", "Mathematics");
                        break;
                    case 2:
                        bundle.putString("key", "Science");
                        break;
                    case 3:
                        bundle.putString("key", "English");
                        break;
                    case 4:
                        bundle.putString("key", "Social Studies");
                        break;
                    case 5:
                        bundle.putString("key", "Foreign Language");
                        break;
                }

                ContactSelectorFragment frag = new ContactSelectorFragment();
                frag.setArguments(bundle);

                transaction.replace(R.id.container_layout, frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

    private class DepartmentListAdapter extends ArrayAdapter<String> {

        private ArrayList<String> items;

        public DepartmentListAdapter(Context context, int textViewResourceId, ArrayList<String> items)
        {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public int getCount() { return items.size(); }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent){
            LayoutInflater vi = (LayoutInflater) DepartmentSelectorFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.standard_row, null);

            ((TextView) v.findViewById(R.id.textView)).setText(items.get(position));
            return v;
        }
    }
}
